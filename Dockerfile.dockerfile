FROM python:3.12
WORKDIR /laba№3
COPY requirements.txt
RUN pip install --no-cache-dir -r requirements.txt
COPY ./laba№3.py
CMD ["python", "laba№3.py"]
