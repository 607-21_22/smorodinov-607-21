import asyncio
import aiohttp
import time
from typing import Optional

start_time = time.time()
all_data = []


async def get_page_data(page_id: int, session: aiohttp.ClientSession) -> Optional[str]:
    if page_id:
        url = f'https://nizhny-novgorod.dacartur.com/avto?sort=popular&page={page_id}'
    else:
        url = f'https://nizhny-novgorod.dacartur.com/avto'
    print(f'Получение данных по URL: {url}')

    async with session.get(url) as response:
        if response.status == 200:
            return await response.text()
        else:
            print(f"Failed to fetch data from URL: {url}")
            return None


async def load_site_data():
    async with aiohttp.ClientSession() as session:
        tasks = []

        for page_id in range(100):
            tasks.append(get_page_data(page_id, session))

        all_data.extend(filter(None, await asyncio.gather(*tasks)))


def save_to_file(file_path: str, data: list):
    with open(file_path, 'w', encoding='utf-8') as file:
        file.write('\n'.join(data))


if __name__ == '__main__':
    asyncio.run(load_site_data())

    end_time = time.time() - start_time
    print(f"\nВремя выполнения: {end_time} секунд")

    save_to_file('parsing.txt', all_data)
    print("Данные сохранены в файл 'parsing.txt'")
