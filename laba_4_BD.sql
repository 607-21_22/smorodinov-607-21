-- 1. Создание таблиц:

-- Создание таблицы для класса Автомобиль (Car)
CREATE TABLE Car (
    carID INT PRIMARY KEY,
    brand VARCHAR(255),
    model VARCHAR(255),
    year INT,
    license_plate VARCHAR(255),
    daily_rate DECIMAL(10, 2),
    available BOOLEAN,
    CHECK (available IN (0, 1))
);

-- Создание таблицы для класса Клиент (Client)
CREATE TABLE Client (
    clientID INT PRIMARY KEY,
    name VARCHAR(255),
    surname VARCHAR(255),
    phone VARCHAR(15),
    email VARCHAR(255),
    password VARCHAR(255)
);

-- Создание таблицы для класса Бронирование (Reservation)
CREATE TABLE Reservation (
    client INT,
    car INT,
    start_date DATE,
    end_date DATE,
    total_cost DECIMAL(10, 2),
    FOREIGN KEY (client) REFERENCES Client(clientID),
    FOREIGN KEY (car) REFERENCES Car(carID)
);

-- 2. Заполнение таблиц значениями:

-- Заполнение таблицы Car
INSERT INTO Car (carID, brand, model, year, license_plate, daily_rate, available)
VALUES
(1, 'Toyota', 'Camry', 2020, 'ABC123', 50.00, TRUE),
(2, 'Honda', 'Civic', 2019, 'XYZ789', 45.00, TRUE);

-- Заполнение таблицы Client
INSERT INTO Client (clientID, name, surname, phone, email, password)
VALUES
(1, 'John', 'Doe', '+123456789', 'john.doe@example.com', 'password123'),
(2, 'Jane', 'Smith', '+987654321', 'jane.smith@example.com', 'pass456');

-- Заполнение таблицы Reservation
INSERT INTO Reservation (client, car, start_date, end_date, total_cost)
VALUES
(1, 1, '2023-01-01', '2023-01-05', 200.00),
(2, 2, '2023-02-01', '2023-02-10', 400.00);

-- 3. Запросы с объединением таблиц:

-- INNER JOIN
SELECT * FROM Reservation
JOIN Client ON Reservation.client = Client.clientID
JOIN Car ON Reservation.car = Car.carID;

-- LEFT JOIN
SELECT * FROM Client
LEFT JOIN Reservation ON Client.clientID = Reservation.client;

-- RIGHT JOIN
SELECT * FROM Reservation
RIGHT JOIN Car ON Reservation.car = Car.carID;

-- FULL OUTER JOIN (используя LEFT JOIN и UNION ALL)
SELECT * FROM Client
LEFT JOIN Reservation ON Client.clientID = Reservation.client
UNION ALL
SELECT * FROM Reservation
LEFT JOIN Client ON Reservation.client = Client.clientID;

-- 4. Полное объединение без использования FULL OUTER JOIN:

-- Полное объединение (FULL OUTER JOIN) без использования UNION ALL
SELECT * FROM Client
LEFT JOIN Reservation ON Client.clientID = Reservation.client
UNION
SELECT * FROM Reservation
LEFT JOIN Client ON Reservation.client = Client.clientID;

-- 5. Запрос исключающий строки с NULL при LEFT/RIGHT/FULL JOIN:

-- Исключение строк с NULL при LEFT JOIN
SELECT * FROM Client
LEFT JOIN Reservation ON Client.clientID = Reservation.client
WHERE Reservation.client IS NOT NULL;

-- Исключение строк с NULL при RIGHT JOIN
SELECT * FROM Reservation
RIGHT JOIN Car ON Reservation.car = Car.carID
WHERE Reservation.car IS NOT NULL;

-- Исключение строк с NULL при FULL OUTER JOIN (используя UNION ALL)
SELECT * FROM Client
LEFT JOIN Reservation ON Client.clientID = Reservation.client
WHERE Reservation.client IS NOT NULL
UNION ALL
SELECT * FROM Reservation
LEFT JOIN Client ON Reservation.client = Client.clientID
WHERE Client.clientID IS NOT NULL;